<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>This is an awesome product</title>
    <link rel="stylesheet" type="text/css" href="min/style.min.css">
    <script src="/UserCommentsModule/UserComments.js"></script>
</head>
<body>
    <header class="ipp-Header">Branding, Main Navigation, Search, Account Settings/Info</header>

    <article class="ipp-Product">
        <h1 class="uc-SingleItem_Title">Sugar Skull</h1>
        Individual Item Information, Gallery, Applied Facets, Shopping Elements
    </article>

    <aside class="uc-UserComments">
        <section class="uc-Previous">
            <h1 class="uc-Previous_Title">Comments</h1>
            <ul class="uc-Previous_List">
                <li class="uc-Previous_Comment">Interesting use of fibonacci. .  .   .</li>
                <li class="uc-Previous_Comment">Can I order in volume? How many can I get by next week?</li>
            </ul>
        </section>
        <section class="uc-Submit">
            <h1 class="uc-Submit_Title">Have something to say?</h1>
            <form class="uc-Submit_Form" action="/api/comment/add" method="post">
                <label class=uc-Submit_Label" for="comment">Submit your comment</label>
                <textarea class=uc-Submit_Textarea" id="comment" name="comment"></textarea>

                <label class=uc-Submit_Label" for="no-captcha">Google No-Captcha integration</label>
                <input class=uc-Submit_Captcha" type="checkbox" id="no-captcha" name="no-captcha"/>

                <input class="uc-Submit_FormButton" type="submit" value="submit" />
            </form>
        </section>
    </aside>


    <footer class="prd-Footer">
        <span class="prd-Footer_Copyright">Copyright &copy; <?php echo date( 'Y' ); ?>
    </footer>
</body>
</html>