var UserComments = (function () {
    const key = '$hapevv@yS';
    let xhr = new XMLHttpRequest();
    // Get user ID
    const user_id = 2;
    // Get product ID
    const product_id = 3;
    // Get new comment
    const text = 'This is mocked for simplicity';

    return {
        // Send via AJAX
        post_comment: function () {
            let query_string;
            let post_data = [];
            const data = {
                'key': key,
                'user_id': user_id,
                'product_id': product_id,
                'text': text
            };

            for (let k in data) {
                post_data.push(k + '=' + data[k]);
            }

            query_string = post_data.join('&');

            xhr.open('POST', '/mock-response.php');
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function () {
                const json_response = JSON.parse(xhr.responseText);
                if (xhr.status === 200 && json_response.success) {
                    //document.dispatchEvent(commentAddSuccess);
                    console.log(json_response);
                }
                else if (xhr.status !== 200) {
                    //document.dispatchEvent(commentAddFailure);
                    alert('Request failed.  Returned status of ' + xhr.status);
                }
            };

            xhr.send(encodeURI(query_string));
        }
        // Handle response
    }
})(window.UserComments || {});
