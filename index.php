<?php

class Database
{

    private $_connection;
    private static $_instance;
    private $_host = "localhost";
    private $_username = "root";
    private $_password = "00dit050";
    private $_database = "shapeways";

    /**
     *  Get an instance of the Database
     *
     * @return Database
     */
    public static function getInstance() {
        if ( !self::$_instance ) { // If no instance then make one
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    // Constructor
    private function __construct() {
        $this->_connection = new mysqli( $this->_host, $this->_username,
            $this->_password, $this->_database );

        // Error handling
        if ( mysqli_connect_error() ) {
            trigger_error( "Failed to connect to MySQL: " . mysql_connect_error(),
                E_USER_ERROR );
        }
    }

    // Get mysqli connection
    public function getConnection() {
        return $this->_connection;
    }
}

class User
{

    private $user_id;
    private $db;

    public function __construct() {
        $db       = Database::getInstance();
        $this->db = $db->getConnection();
    }

    /* Select queries return a resultset */
    public function unread_comments( $user_id = NULL, $product_id = NULL ) {

        if ( NULL === $user_id || NULL === $product_id )
            return [ 'error' => 'Missing data' ];

        $query = 'SELECT c.id, c.text' .
                 ' FROM `unread_comments` AS `uc`' .
                 ' INNER JOIN `comments` AS `c` ON uc.comment_id = c.id' .
                 ' WHERE uc.user_id = ' . $user_id .
                 ' AND c.product_id = ' . $product_id .
                 ' AND uc.read = 0;';

        if ( $result = $this->db->query( $query ) ) { // Presumes mysqli object exists
            while ( $row = mysqli_fetch_assoc( $result ) ) :
                $response[] = $row;
            endwhile;
        } else {
            $response = 'failed';
            var_export( $result );
        }

        return $response;
    }
}

class Comment
{

    private $db;

    public function __construct() {
        $db       = Database::getInstance();
        $this->db = $db->getConnection();
    }

    /**
     * @param int|NULL $user_id
     * @param int|NULL $product_id
     * @param string   $text
     *
     * @return array|bool|\mysqli_result
     */
    public function add( int $user_id = NULL, int $product_id = NULL, $text = '' ) {

        if ( NULL === $user_id || NULL === $product_id || '' === $text )
            return [ 'error' => 'Missing data' ];

        $clean_text = htmlspecialchars($text);

        $query = 'INSERT INTO `comments`' .
                 ' (user_id, product_id, text) VALUES' .
                 ' (' . $user_id . ', ' . $product_id .', "' . $clean_text . '");';

        return $this->db->query( $query ); // Presumes mysqli object exists
    }
}

//$db     = Database::getInstance();
//$mysqli = $db->getConnection();
//$query  = 'SELECT * FROM unread_comments WHERE user_id = 1;';
//$result = $mysqli->query( $query );
//
//$user     = new User();
//$comments = $user->unread_comments( 0, 0 );

$comment = new Comment();
$well = $comment->add( 0, 0, 'This is dynamic');
var_export($well);
