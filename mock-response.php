<?php
/**
 * This entire file is a mock of a larger suite of tools that would connect
 * to a database, handle routing, provide security, validation and
 * sanitization, and ultimately provide the JSON object for the API response.
 *
 * This is mocked for the purposes of demonstrating AJAX behavior in a
 * javascript file for the purposes of submitting a comment on a Shapeways
 * product page.
 */

// Accept payload
$request = new Request();
if ( !Api::is_ajax( $request->get_post_data() ) )
    die( 'Forbidden' );

// Save to DB
$comment = new Comment;
$new_id  = $comment->add( $request->get_post_data() );

// Get new comment from DB
if ( !empty( $new_id[ 'error' ] ) )
    Response::to_json( $new_id[ 'error' ], 'Failed to add comment', 500 );

$data = $comment->get( $new_id );

// Send response
Response::to_json( $data, 'Comment added' );

class Request
{

    public function get_post_data() {
        if ( empty( $_POST ) )
            return [ 'error' => 'Empty POST data' ];

        return (object) $_POST;
    }
}

class Api
{

    private static $api_key = '$hapevv@yS';

    public static function is_ajax( $request ) {

        if ( empty( $request->key ) || self::$api_key !== $request->key )
            return FALSE;

        return TRUE;
    }
}

class Comment
{

    private $db;

    public function __construct() {
        $db       = Database::getInstance();
        $this->db = $db->getConnection();
    }

    /**
     * @param \stdClass $data
     *
     * @return array|mixed
     */
    public function add( stdClass $data ) {

        $validate = new Validate();
        $validate->comment( $data );

        if ( !$validate->is_valid() )
            return [ 'error' => [ $validate->get_errors() ] ];

        $clean_text = htmlspecialchars( $data->text );

        $query = 'INSERT INTO `comments`' .
                 ' (user_id, product_id, text) VALUES' .
                 ' (' .
                 (int) $data->user_id . ', ' .
                 (int) $data->product_id . ', "' .
                 $clean_text .
                 '");';

        try {
            if ( !$this->db->query( $query ) )
                throw new Exception( 'Database insert failed' );
        }
        catch ( Exception $e ) {
            return [ 'error' => $e->getMessage() ];
        }

        return $this->db->insert_id;
    }

    public function get( $id = NULL ) {
        if ( empty( $id ) )
            return;

        $query = 'SELECT * FROM `comments`' .
                 ' WHERE id = ' . $id .
                 ' LIMIT 1;';

        try {
            if ( !$result = $this->db->query( $query ) )
                throw new Exception( 'Could not find comment' );

            while ( $row = mysqli_fetch_assoc( $result ) ) :
                $response[] = $row;
            endwhile;
        }
        catch ( Exception $e ) {
            $response = [ 'error' => $e->getMessage() ];
        }

        return $response;
    }
}

class Validate
{

    private $validation_errors = [];
    private $current_key = NULL;
    private $current_value = NULL;
    private $is_valid = NULL;

    public function comment( $data ) {
        $this->assert( $data, 'user_id', [ 'numeric' ] );
        $this->assert( $data, 'product_id', [ 'numeric' ] );
        $this->assert( $data, 'text', [ 'string' ] );
    }

    private function assert( $data, $key, array $rules ) {

        if ( !property_exists( $data, $key ) )
            return $this->validation_errors[] = [ $key => 'Property not set' ];
        $this->current_key   = $key;
        $this->current_value = $data->{$key};
        foreach ( $rules as $rule ) :
            switch ( $rule ) :
                case 'numeric' :
                    $this->is_numeric();
                    break;
                case 'string' :
                    $this->is_string();
                    break;
                default :
                    $this->validation_errors[] = [
                        $key => [
                            $rule . ' validation rule does not exist',
                        ],
                    ];

                    return FALSE;
            endswitch;
        endforeach;
    }

    private function is_numeric() {
        if ( !is_numeric( $this->current_value ) )
            $this->push_error( 'is_numeric', FALSE );;
    }

    private function is_string() {
        if ( !is_string( $this->current_value ) )
            $this->push_error( 'is_string', FALSE );
    }

    private function push_error( $rule = NULL, $bool = NULL ) {
        if ( !is_string( $rule ) || !is_bool( $bool ) )
            throw new Exception();
        $this->validation_errors[] = [
            $this->current_key => [
                $rule => $bool,
            ],
        ];
    }

    public function get_errors() {
        return $this->validation_errors;
    }

    public function is_valid() {
        if ( 0 < count( $this->validation_errors ) || $this->is_valid = NULL )
            return FALSE;

        return TRUE;
    }
}

class Database
{

    private $_connection;
    private static $_instance;
    private $_host = "localhost";
    private $_username = "root";
    private $_password = "00dit050";
    private $_database = "shapeways";

    // Constructor
    private function __construct() {
        $this->_connection = new mysqli( $this->_host, $this->_username,
            $this->_password, $this->_database );

        // Error handling
        if ( mysqli_connect_error() ) {
            trigger_error( "Failed to connect to MySQL: " . mysql_connect_error(),
                E_USER_ERROR );
        }
    }

    /**
     *  Get an instance of the Database
     *
     * @return Database
     */
    public static function getInstance() {
        if ( !self::$_instance ) { // If no instance then make one
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    // Get mysqli connection
    public function getConnection() {
        return $this->_connection;
    }
}

class Response
{

    /**
     * This method should ultimately handle objects, arrays, and strings.
     * Object implemented for this mock.
     *
     * By default we want to terminate the request for API access to prevent
     * any further script processing after sending the response.
     *
     * @param      $data
     * @param bool $terminate
     */

    public static function to_json(
        $data,
        $message = "",
        $code = 200,
        $terminate = TRUE
    ) {

        $status = [
            200 => '200 OK',
            400 => '400 Bad Request',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error',
        ];

        header_remove();
        http_response_code( $code );
        header( 'Content-Type: application/json' );
        header( 'Status: ' . $status[ $code ] );

        $response = [
            'status'  => $code,
            'success' => $code < 300,
            'message' => $message,
            'data'    => $data,
        ];
        echo json_encode( $response );

        if ( $terminate )
            die();
    }
}